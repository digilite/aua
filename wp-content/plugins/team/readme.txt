=== Team ===
	Contributors: pickplugins
	Donate link: http://pickplugins.com
	Tags: team, members profiles, our teams, team members, team plugin wordpress, meet the team
	Requires at least: 3.8
	Tested up to: 4.9
	Stable tag: 3.2.39
	License: GPLv2 or later
	License URI: http://www.gnu.org/licenses/gpl-2.0.html

	Fully responsive and mobile ready meet the team showcase plugin for wordpress.

== Description ==


Team is pure html & css3 responsive meet the team grid for wordpress.
Display team member profiles with descriptions and links to social media using our shortcode, widget. this plugin also support themes.

### Team by http://pickplugins.com

* [Upgrade to Premium!&raquo;](http://pickplugins.com/items/team-responsive-meet-the-team-grid-for-wordpress/)
* [Watch Video Tutorial!&raquo;](https://www.youtube.com/watch?v=8OiNCDavSQg)


<strong>Plugin Features</strong><br />

* Fully responsive and mobile ready.
* Query Team meamber from latest, Older Published, by Only Year, by Month of a year, Categories, post id.
* Unlimited team's anywhere.
* Two Different themes.
* Custom number of member query.
* Team member thumbnail images size selection.
* Team grid thumbnail custom width.
* Team grid thumbnail custom height.
* Team grid items text align.
* Background image for team area.
* Team member name font color.
* Team member name font size.
* Team member position font color.
* Team member position font size.
* Team member bio font color.
* Team member bio font size.


<strong>Premium Features</strong><br />

* 30+ Ready Different Skin.
* Display from member categories(group).
* Display by member id’s.
* Custom icons for custom social links.
* Click-able link to custom post team members.
* Click-able link to custom link to team members.
* Popup profile box on click member thumbnail.
* Popup Slider on click member.
* Mixitup style team grid.
* Skill bars.
* Short-code support for team member content.


== Installation ==

1. Install as regular WordPress plugin.<br />
2. Go your plugin setting via WordPress Dashboard and find "<strong>Team</strong>" activate it.<br />

After activate plugin you will see "Team" menu at left side on WordPress dashboard.<br />

First create some team member from "Team Member" you can assign categories(Team Group) for members.

After creating some team members you need to generate shortcode to display team grid on page.
see the menu on left "Team" click "New Team" and choose option as you need, and click "Team Content" nav to select team members by various type query as you need.

<br />
<strong>How to use on page or post</strong><br />
When Team options setup done please publish "Team" as like post or page<br />

and then copy shortcode from top of <strong>Team Options</strong> `[team  id="1234" ]`<br />

then paste this shortcode anywhere in your page to display Team gird.<br />







== Screenshots ==

1. screenshot-1
2. screenshot-2
3. screenshot-3
4. screenshot-4
5. screenshot-5

== Changelog ==

	=3.2.39=
    * 20/09/2018 add - Slider option added

	=3.2.38=
    * 10/09/2018 fix - minor bug fixed

	=3.2.36=
    * 28/04/2018 fix - Single team member page skill sorting.

	=3.2.35=
    * 28/04/2018 fix - Single team member content format
    * 28/04/2018 fix - Social icon visibility error issue fixed.

	=3.2.33=
    * 12/03/2018 add - hover title text on social icons


	=3.2.32=
    * 11/03/2018 add - single team member meta info added

	=3.2.31=
    * 07/02/2018 fix - filterable style grid width issue fixed

	=3.2.30=
    * 07/02/2018 add - display pagination at top.
    * 07/02/2018 add - filter hook for skill on grid added.
    * 07/02/2018 add - custom html under nth item on grid.


	=3.2.29=
    * 29/01/2018 add - Popup thumbnail source.

	=3.2.27=
    * 21/01/2018 fix - single team member sidebar issue fixed.

	=3.2.26=
    * 17/12/2017 fix - safari browser issue update.

	=3.2.25=
    * 31/03/2017 add - new shortcode to avoid conflict.

	=3.2.24=
    * 23/03/2017 add - close button on slider popup.
	
	=3.2.23=
    * 21/03/2017 add - Orrder by meta value.
    * 21/03/2017 update - query member update.	

	=3.2.22=
    * 22/02/2017 update - Empty check for meta key value.

	=3.2.21=
    * 30/01/2017 update - admin option update.

	=3.2.20=
    * 21/01/2016 fix - team member bio text issue fixed.

	=3.2.19=
    * 30/12/2016 add - Query orderby menu order.

	=3.2.18=
    * 14/11/2016 fix - pagination issue fixed for filtarable grid.

	=3.2.17=
    * 14/11/2016 add - custom meta fields wrapper.

	=3.2.16=
    * 07/11/2016 add - Added FAQ on help page.
	
	=3.2.15=
    * 05/11/2016 add - added some filter hook for single team member page.

	=3.2.14=
    * 17/10/2016 add - translation added.

	=3.2.13=
    * 12/10/2016 add - image size for popup thumbnail.

	=3.2.12=
    * 30/09/2016 fix - social icons visibility issue fixed.
    * 30/09/2016 fix - team member content issue fixed.	
    * 30/09/2016 add - social input vimeo added.
    * 30/09/2016 add - social input instagram added.	
	
	=3.2.11=
    * 03/09/2016 add - scroll top to click jQuery pagination of flitarable grid.

	=3.2.10=
    * 31/08/2016 fix - team member single page update.

	=3.2.9=
	
    * 14/08/2016 fix - social icon in single team member.

	=3.2.8=
    * 27/07/2016 add - added filter hook for popup thumbnail.

	=3.2.7=
    * 23/07/2016 fix - team member postion on popup slider.

	=3.2.6=
    * 18/07/2016 fix - some minor issue fixed

	=3.2.5=
    * 04/07/2016 fix - team skin filter issue fixed.

	=3.2.4=
    * 21/06/2016 add - Filterable navs background color.
    * 21/06/2016 add - Filterable navs text color.	
    * 21/06/2016 add - Filterable navs hover color.	

	=3.2.3=
    * 21/06/2016 update - update mixitup.js

	=3.2.2=
    * 17/06/2016 fix - Invalid argument supplied for foreach in team member edit page issue fixed.
    * 17/06/2016 fix - responsive issue fixed.

	=3.2.1=
    * 16/06/2016 add - Popup Slider.

	=3.2.0=
    * 03/06/2016 add - added 30+ skin.
    * 03/06/2016 add - 3 style grid, filtrable, slider.
    * 03/06/2016 add - popup slider.
    * 03/06/2016 add - new thumbnail size team-500px added.		

	=3.1.17=
    * 06/05/2016 fix - popup box member position issue fixed.

	=3.1.16=
    * 05/05/2016 fix - license menu broken issue fixed.


	=3.1.15=
    * 10/03/2016 add - slider options added.
	
	=3.1.14=
    * 07/03/2016 add - slider themes items hide issue fixed.

	=3.1.13=
    * 23/02/2016 add - filter hook for popup box.
	
	=3.1.12=
    * 04/02/2016 fix - team member slug issue fixed.
	
	=3.1.11=
    * 27/01/2016 add - shortcode support for team member content.

	=3.1.10=
    * 25/01/2016 update - settings UI update.

	=3.1.9=
    * 15/01/2016 fix - empty skill issue fixed.
	
	=3.1.8=
    * 04/01/2016 update - minor issue update.

	=3.1.7=
    * 04/11/2015 add - masonry issue fixed.

	=3.1.6=
    * 23/10/2015 add - team member custom slug.

	=3.1.5=
    * 19/10/2015 add - new themes added list style.
	
	=3.1.4=
    * 16/10/2015 add - grid item width for tablet.

	=3.1.3=
    * 06/10/2015 add - Single team member profile.

	=3.1.2=
    * 04/10/2015 add - social input fields improved.
	
	=3.1.1=
    * 22/09/2015 add - css issue fixed for left-right theme.
	
	= 3.1.0=
    * 18/08/2015 add - add social field for phone.

	= 3.0.10=
    * 11/07/2015 fix - hyperlink open new tab issue for social icons.

	= 3.0.9=
    * 11/07/2015 add - grid item width in mobile device.
    * 11/07/2015 add - popup close button issue fixed.
	
	= 3.0.8=
    * 03/07/2015 fix - output bug fixed.
	
	= 3.0.7=
    * 27/06/2015 add - add filter for social fields.
    * 27/06/2015 add - add linkedin on social fields.

	= 3.0.6=
    * 20/06/2015 add - re-write plugin code and made by OOP.
	* 20/06/2015 add - add filter & action hook to make extensible some part of plugin.
	* 20/06/2015 add - remove some themes.
	* 20/06/2015 fix - fixed some minor CSS.
	* 20/06/2015 fix - added isotope filterable style grid.
	* 20/06/2015 add - add sortable grid items by grid builder.

	= 3.0.5=
    * 24/05/2015 fix - li, ul element covert to div.
    * 24/05/2015 fix - zoomin, zoomout, flip-x, flip-y theme popup box issue fixed.	
	
	= 3.0.4=
    * 19/05/2015 fix - php error fixed.

	= 3.0.3=
    * 16/05/2015 add - popup box linked to team member name(post title).

	= 3.0.2=
    * 15/05/2015 add - MixitUp Default active filter.
	
	= 3.0.1=
    * 10/05/2015 add - new theme thumbnail on left.
    * 10/05/2015 add - new theme thumbnail on left right.
    * 10/05/2015 add - load only yout selected themes for optimizing site loaidng.

	= 3.0.0=
    * 05/05/2015 add - Mixitup style team grid.

	= 2.10=
    * 30/04/2015 remove - remove option filter content "older published member".
    * 30/04/2015 add - Post query order.
	
	
	= 2.9=
    * 29/04/2015 add - Read more link to popup.
    * 29/04/2015 add - close button for popup box.


	= 2.8=
    * 20/04/2015 add - Pagination support.
    * 20/04/2015 add - Multisite Domain licensed.

	= 2.7=
    * 07/04/2015 add - Display full content or excerpt for popup box.
    * 07/04/2015 add - Custom excerpt Length for popup box.
    * 07/04/2015 add - Custom read more Text for popup box.

	= 2.6=
    * 01/04/2015 improve- improve social icons image.
    * 01/04/2015 add- 4 different social icon style.
	
	= 2.5=
    * 28/03/2015 add- added social field for mobile/telephone muber.

	= 2.4=
    * 28/03/2015 add- Drag & Drop social icon sorting.
    * 28/03/2015 add- Remove social icons anytime.
    * 28/03/2015 add- Add your own icons for website, email, skype.
	
	= 2.3=
    * 09/03/2015 add- License for plugin.

	= 2.2=
    * 08/03/2015 add- New theme flip-y.
    * 08/03/2015 add- New theme Zoom In.	
    * 08/03/2015 add- New theme Zoom Out.
	
	= 2.1=
    * 06/03/2015 add- New theme flip-x.

	= 2.0=
    * 04/03/2015 fix - popup profile responsive issue.
    * 24/02/2015 add - Masonry Style grid.
    * 24/02/2015 remove - height for content.
    * 24/02/2015 add - display full content or excerpt.
    * 24/02/2015 add - Excrept Length.
    * 24/02/2015 add - custom read more Text for excerpt.

	= 1.10 =
	
    * 29/01/2015 fix - loop error issue fixed.

	= 1.9 =
	
    * 17/01/2015 added - Popup profile box on click member thumbnail.

	= 1.8 =
	
    * 11/01/2015 added - New Theme added.


	= 1.7 =
	
    * 26/12/2014 added - added theme support function.
    * 26/12/2014 added - remove help page added help tab on setting page.
	
	= 1.6 =
	
    * 17/12/2014 added - added option for link to custom post team member.

	= 1.5 =
	
    * 09/12/2014 added input field for custom field icons.
	
	= 1.4 =
	
	* 20/11/2014 added new themes.

	
	= 1.3 =
	
	* 15/11/2014 php error fixed.
	* 15/11/2014 js error fixed.
	* 15/11/2014 Responsive admin.
	* 15/11/2014 remove help page and added help tab.
	
	= 1.2 =
	
    * 28/10/2014 added input field for grid items align.
    * 28/10/2014 added input field for member bio height.
    * 30/10/2014 Responsive admin settings.
    * 25/10/2014 added input field for margin to grid items.
	
		
	= 1.1 =
	
    * 01/10/2014 added email link to profile.
    * 01/10/2014 added website link to profile.
    * 01/10/2014 Fix problem to display member position.	
	
	= 1.0 =
	
    * 06/09/2014 Initial release.
