<?php

get_header();

global $mo_theme;

$number_of_columns = 4;
$image_size = 'small';
$excerpt_count = 138;

$show_content = false;

$style_class = mo_get_column_style($number_of_columns);

$layout_mode = mo_get_theme_option('mo_course_layout_mode', 'fitRows');
if ($layout_mode == 'masonry')
    $image_size = 'proportional';

mo_exec_action('before_content');
?>

    <div id="content" class="<?php echo mo_get_content_class(); ?>">

        <?php mo_exec_action('start_content'); ?>

        <div class="hfeed">

            <?php

            $mo_theme->set_context('loop', 'custom'); // for pagination

            if (is_page_template('template-courses.php')) {

                mo_show_page_content();

            }

            if (is_front_page()) {
                $paged = (get_query_var('page')) ? get_query_var('page') : 1;
            }
            else {
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            }

            $query_args = array(
                'post_type' => 'course',
                'posts_per_page' => 8,
                'paged' => $paged,
                'orderby' => 'menu_order',
                'order' => 'ASC'
            );

            $term = get_term_by('slug', get_query_var('term'), 'course_category');

            if ($term) {
                $query_args['tax_query'] = array(
                    array(
                        'taxonomy' => 'course_category',
                        'field' => 'slug',
                        'terms' => $term
                    )
                );
                $query_args['posts_per_page'] = get_option('posts_per_page');
            }

            $loop = new WP_Query($query_args);

            if ($loop->have_posts()) :

                echo mo_get_post_type_archive_links('course', 'course_category');

                echo '<ul class="image-grid post-snippets ' . $layout_mode . ' js-isotope" data-isotope-options=\'{ "itemSelector": ".entry-item", "layoutMode": "' . $layout_mode . '" }\'>';

                while ($loop->have_posts()) : $loop->the_post();

                    mo_exec_action('before_entry'); ?>

                    <li data-id="<?php echo get_the_ID(); ?>" class="entry-item <?php echo $style_class; ?>">

                        <article id="post-<?php echo get_the_ID(); ?>"
                                 class="<?php echo join(' ', get_post_class()); ?> clearfix">

                            <?php

                            mo_exec_action('start_entry');

                            ?>

                            <div class="entry-snippet">

                                <?php

                                echo '<div class="img-wrap">';

                                $thumbnail_exists = mo_thumbnail(array(
                                    'image_size' => $image_size,
                                    'wrapper' => false
                                ));

                                if ($thumbnail_exists)
                                    echo '<div class="image-overlay"></div>';

                                $post_title = get_the_title();
                                $post_link = get_permalink();

                                echo mo_get_type_info();

                                echo '</div><!-- .img-wrap -->';

                                echo '<div class="entry-text-wrap' . ($thumbnail_exists ? '' : ' nothumbnail') . '">';

                                $course_id = get_post_meta(get_the_ID(), 'mo_course_identifier', true);
                                if (!empty($course_id)) {
                                    echo '<div class="course-id">' . $course_id . '</div>';
                                }

                                echo mo_get_taxonomy_info('course_category');

                                echo the_title('<h2 class="entry-title"><a href="' . get_permalink() . '" title="' . get_the_title() . '" rel="bookmark">', '</a></h2>', false);

                                echo '<div class="entry-summary">';

                                if ($show_content) {
                                    global $more;
                                    $more = 0;
                                    /*TODO: Remove the more link here since it will be shown later */
                                    the_content(__('Read More <span class="meta-nav">&rarr;</span>', 'mo_theme'));
                                }
                                else {
                                    echo mo_truncate_string(get_the_excerpt(), $excerpt_count);
                                }

                                echo '</div>'; // .entry-summary

                                echo '</div><!-- .entry-text-wrap -->';

                                ?>

                            </div>
                            <!-- .entry-snippet -->

                            <?php mo_exec_action('end_entry'); ?>

                        </article>
                        <!-- .hentry -->

                    </li><!-- .isotope element -->

                    <?php mo_exec_action('after_entry');

                endwhile;

                echo '</ul><!-- post-snippets -->';

                mo_exec_action('end_content');

                include(locate_template('loop-nav.php'));

            else :

                get_template_part('loop-error'); // Loads the loop-error.php template.

            endif; ?>

        </div>
        <!-- .hfeed -->

    </div><!-- #content -->

<?php mo_exec_action('after_content');

wp_reset_postdata(); /* Right placement to help not lose context information */

$mo_theme->set_context('loop', null); //reset it

get_sidebar();

get_footer();