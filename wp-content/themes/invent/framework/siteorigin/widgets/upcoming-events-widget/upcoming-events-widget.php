<?php

/*
Widget Name: Upcoming events
Description: Displays a list of events. These events are entered by creating Event custom post types in the Events tab of the WordPress Admin.
Author: LiveMesh
Author URI: https://www.livemeshthemes.com
*/


class MO_Upcoming_Events_Widget extends SiteOrigin_Widget {
    function __construct() {
        parent::__construct(
            "mo-upcoming-events",
            __("Upcoming events", "mo_theme"),
            array(
                "description" => __("Displays a list of events. These events are entered by creating Event custom post types in the Events tab of the WordPress Admin.", "mo_theme"),
                "panels_icon" => "dashicons dashicons-minus",
            ),
            array(),
            array(
                "title" => array(
                    "type" => "text",
                    "label" => __("Title", "mo_theme"),
                    "default" => __("Upcoming Events", "mo_theme"),
                ),
                "class" => array(
                    "type" => "text",
                    "description" => __("The class name to be inserted for the element wrapping the list of posts displayed. Default: simple-list", "mo_theme"),
                    "label" => __("Class", "mo_theme"),
                    "default" => __("simple-list", "mo_theme"),
                ),
                "post_count" => array(
                    "type" => "number",
                    "description" => __("The number of events to be displayed. ", "mo_theme"),
                    "label" => __("Post count", "mo_theme"),
                    "default" => 4,
                ),
                "category_ids" => array(
                    "type" => "text",
                    "description" => __("A comma separated category ids of the Event custom post types created in the Events tab of the WordPress Admin. Helps to filter the events for display (optional)", "mo_theme"),
                    "label" => __("Category Ids", "mo_theme"),
                    "default" => "",
                ),
                "all_events_link" => array(
                    "type" => "checkbox",
                    "description" => __("Specify if a link to all events must be displayed below the list of events.", "mo_theme"),
                    "label" => __("Show link to all events page?", "mo_theme"),
                    "default" => true,
                ),
                "no_upcoming_events" => array(
                    "type" => "checkbox",
                    "description" => __("Applies only when there are no upcoming events found. If set to true, no message will be displayed indicating that no upcoming events were found.", "mo_theme"),
                    "label" => __("Indicate No Upcoming Events?", "mo_theme"),
                    "default" => false,
                ),
            )
        );
    }

    function get_template_variables($instance, $args) {
        return array(
            "class" => $instance["class"],
            "post_count" => $instance["post_count"],
            "category_ids" => $instance["category_ids"],
            "all_events_link" => $instance["all_events_link"],
            "no_upcoming_events" => $instance["no_upcoming_events"],
        );
    }

}
siteorigin_widget_register("mo-upcoming-events", __FILE__, "MO_Upcoming_Events_Widget");