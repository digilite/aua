<?php
/**
 * @var $class
 * @var $post_count
 * @var $category_ids
 * @var $all_events_link
 * @var $no_upcoming_events
 */


echo do_shortcode('[upcoming_events class="' . $class . '" post_count="' . $post_count . '" category_ids="' . $category_ids . '" all_events_link="' . $all_events_link . '" no_upcoming_events="' . $no_upcoming_events . '" ]');