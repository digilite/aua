<?php
/**
 * @var $title
 * @var $number_stats
 */

$shortcode = '[number-stats]';

$count = 0;

foreach ($number_stats as $number_stat):

    $count = $count + 1;

    if ($count % 3 == 0)
        $column_shortcode = 'four_col_last';
    else
        $column_shortcode = 'four_col';

    $icon_image_url = siteorigin_widgets_get_attachment_image_src(
        $number_stat['icon_image_url'],
        'full',
        !empty($number_stat['icon_image_url_fallback']) ? $number_stat['icon_image_url_fallback'] : ''
    );
    if (!empty($icon_image_url))
        $icon_image_url = $icon_image_url[0];

    $shortcode .= '[' . $column_shortcode . ']';

    $shortcode .= '[number-stat title="' . $number_stat['title'] . '" text="' . $number_stat['text'] . '" icon="' . $number_stat['icon'] . '" icon_image_url="' . $icon_image_url . '"]';

    $shortcode .= $number_stat['value'];

    $shortcode .= '[/number-stat]';

    $shortcode .= '[/' . $column_shortcode . ']';

endforeach;

$shortcode .= '[/number-stats]';

echo do_shortcode($shortcode);