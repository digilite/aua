<?php

/*
Widget Name: Number stats
Description: Displays a collection of numbers to indicate a statistic in the stats section.
Author: LiveMesh
Author URI: https://www.livemeshthemes.com
*/


class MO_Number_Stats_Widget extends SiteOrigin_Widget {
    function __construct() {
        parent::__construct(
            "mo-number-stats",
            __("Number stats", "mo_theme"),
            array(
                "description" => __("Displays a collection of numbers to indicate a statistic in the stats section..", "mo_theme"),
                "panels_icon" => "dashicons dashicons-minus",
            ),
            array(),

            array(
                'title' => array(
                    'type' => 'text',
                    'label' => __('Title', 'mo_theme'),
                ),

                'number_stats' => array(
                    'type' => 'repeater',
                    'label' => __('Number Stats', 'mo_theme'),
                    'item_name' => __('Number Stat', 'mo_theme'),
                    'item_label' => array(
                        'selector' => "[id*='title']",
                        'update_event' => 'change',
                        'value_method' => 'val'
                    ),
                    'fields' => array(

                        "title" => array(
                            "type" => "text",
                            "description" => __("The title indicating the stats title.", "mo_theme"),
                            "label" => __("Stats title", "mo_theme"),
                            "default" => __("Web Design 87%", "mo_theme"),
                        ),
                        "value" => array(
                            "type" => "number",
                            "description" => __("The percentage value for the stats to be displayed.", "mo_theme"),
                            "label" => __("Value", "mo_theme"),
                            "default" => 87,
                        ),
                        "text" => array(
                            "type" => "text",
                            "description" => __("Any additional text shown below the title.", "mo_theme"),
                            "label" => __("Text", "mo_theme"),
                            "default" => "",
                        ),
                        "icon" => array(
                            "type" => "text",
                            "description" => __("The font icon to be displayed for the statistic being displayed, chosen from the list of icons listed at https://www.livemeshthemes.com/support/faqs/how-to-use-1500-icons-bundled-with-the-agile-theme/", "mo_theme"),
                            "label" => __("Icon", "mo_theme"),
                            "default" => __("icon-world", "mo_theme"),
                        ),
                        "icon_image_url" => array(
                            "type" => "media",
                            "library" => "image",
                            "description" => __("The image icon for the stats. Can be useful when suitable font icon is not available.", "mo_theme"),
                            "label" => __("Icon Image", "mo_theme"),
                            "fallback" => true,
                        ),
                    )
                ),

            )
        );
    }

    function get_template_variables($instance, $args) {
        return array(
            "title" => $instance["title"],
            'number_stats' => !empty($instance['number_stats']) ? $instance['number_stats'] : array(),
        );
    }

}
siteorigin_widget_register("mo-number-stats", __FILE__, "MO_Number_Stats_Widget");

