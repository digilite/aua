
<?php
/**
 * @var $style
 * @var $class
 * @var $type
 * @var $title
 * @var $pitch_text
 */


echo do_shortcode('['. $type .' style="' . $style . '" class="' . $class . '" title="' . $title . '" pitch_text="' . $pitch_text . '" ]');

