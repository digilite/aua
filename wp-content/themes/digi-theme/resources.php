<?php
/*
* Template Name: Resources
*/

get_header();?>

<main class="resources-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1>Resources</h1>
        <div class="icon-text">
          <div class="icon-text-box">
            <div class="icon-text-icon">
              <img src="<?php echo get_template_directory_uri(); ?>/img/group-1403.png" alt="Icon">
            </div>
            <div class="icon-text-box-info">
              <h5>DATA</h5>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            </div>
          </div><!-- Recource Box -->

          <div class="icon-text-box">
            <div class="icon-text-icon">
              <img src="<?php echo get_template_directory_uri(); ?>/img/group-1403.png" alt="Icon">
            </div>
            <div class="icon-text-box-info">
              <h5>DATA</h5>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            </div>
          </div><!-- Recource Box -->
        </div>
      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>






<?php get_footer(); ?>
