<?php
/*
* Template Name: Collaborations
*/

get_header(); 

$args = array(
  'post_type' => 'post',
  'tax_query' => array(
      array(
          'taxonomy' => 'category',
          'field'    => 'slug',
          'terms'    => 'collaborations',
      ),
  ),
);

?>

<main class="collaborations-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php the_title(); ?></h1>

        <?php if( have_rows('collaborations') ): ?>
          <section class="title-with-links">
          <?php while( have_rows('collaborations') ): the_row(); 
              $single_collab_title = get_sub_field('single_collaboration_title');
              ?>
              <div class="single-collab">

              <h3 class="h5"><?= $single_collab_title ?></h3>
              <!-- Inner repeater start -->
              <?php if( have_rows('single_collaboration_links') ): ?>
                  <ul>
                  <?php while( have_rows('single_collaboration_links') ): the_row(); 
                      $text = get_sub_field('text'); 
                      $text_url = get_sub_field('text_url');
                      ?>
                      <li>
                        <a href="<?= esc_url($text_url); ?>" target="_blank"><?= $text ?></a>
                      </li>
                  <?php endwhile; ?>
                  </ul>
              <?php endif; ?>
              <!-- Inner repeater end -->
              </div><!-- Single Colllab -->
          <?php endwhile; ?>
          </section>
        <?php endif; ?>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>






<?php get_footer(); ?>


