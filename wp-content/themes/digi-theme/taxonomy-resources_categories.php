<?php get_header(); 

$tax_name = 'resources_categories';
$the_id = get_queried_object()->term_id;
$termf = get_queried_object();
$parent_id = wp_get_term_taxonomy_parent_id( $the_id, $tax_name );
$template = get_field( 'choose_template', $termf );
$child_term = get_term( $the_id, $tax_name );
$parent_term = get_term( $child_term->parent, $tax_name );
$current_term = single_term_title( "", false );

$smth = get_term( $the_id, $tax_name );
$slug = $smth->slug;

if($_GET['topic'] && !empty($_GET['topic']))
{
    $topic = $_GET['topic'];
}

if($_GET['search_text'] && !empty($_GET['search_text']))
{
    $search_text = $_GET['search_text'];
}

$args = array(
  'post_type' => 'resources_cpt',
  'posts_per_page' => -1,
  's' => $search_text,
  'tax_query' => array(
    'relation' => 'AND',
      array(
         'taxonomy' => $tax_name,
         'field'    => 'slug',
         'terms'    => $slug,
      ),
      array(
        'taxonomy' => 'post_tag',
        'field'    => 'slug',
        'terms'    => $topic,
        'operator' => 'AND',
     ),
    ),

);
$query = new WP_Query($args);


if( $template !== "tablebig" ) : ?>
<main class="collaborations-page ftm">
  <div class="container">
    <div id="plug-breadcrumbs">
      <?php if (function_exists('show_full_breadcrumb')) show_full_breadcrumb(); ?>
    </div>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1>
          <?php if( !empty($parent_term->name) ) {
              echo $parent_term->name;
            } else {
              echo $current_term;
            } ?>
        </h1>

        <?php if( $template !== "dlibraries" ) :
         get_template_part("templates/sub-terms"); 
         get_template_part("templates/filters");
        endif; ?>

        <?php
        if( $template == "table" ) : 
        if ($query->have_posts()) { ?>
          <table class="resource-table">
          <tr class="table-titles">
            <th>Title</th>
            <th>Source</th>
            <th>Year</th>
            <th>Languages</th>
            <th>Keyword</th>
          </tr>
        <?php
          while ($query->have_posts()) :
          $query->the_post();  
          $posttags = get_the_terms( get_the_ID(), 'post_tag' );
          $source = get_field("source");
          $language = get_field("language");
          ?>
          
            <tr>
              <td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
              <td><?= $source ?></td>
              <td><?= get_the_date( 'Y' ); ?></td>
              <td class="lang"><?= $language ?></td>
              <td class="tags">
              <?php
              foreach( $posttags as $tag ) { 
               echo $tag->name.'<span>, </span>';
              } ?>
              </td>
            </tr>
          
          <?php endwhile; ?>
          </table>
        <?php } else {
         echo "<p class='no-data-found'>No data found.</p>"; 
        } wp_reset_postdata();
        endif;



        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
        $argss = array(
          'post_type' => 'resources_cpt',
          'posts_per_page' => 4,
          'paged' => $paged,
          'tax_query' => array(
              array(
                'taxonomy' => $tax_name,
                'field'    => 'slug',
                'terms'    => $slug,
              ),
              )
        );
        $querys = new WP_Query($argss);

        if( $template == "fimage" || $template == "videos" ) :
          if ($querys->have_posts()) : ?>
          <div class="row">
          <?php
          while ($querys->have_posts()) : $querys->the_post();  
          $posttags = get_the_terms( get_the_ID(), 'resources_tag' );
          $source = get_field("source");
          $language = get_field("language");
          $vid_link = get_field("video_link");
          ?>
          <div class="col-md-6">
            <div class="title-ex-fevid">
              <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
              <div class="excerpt">
                <?php the_excerpt(); ?>
              </div>
              <?php
              if( $template == "fimage" ) {
                if( has_post_thumbnail() ) : ?>
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail();?></a>
                <?php endif;
              } else { ?>
                <div class="embed-responsive embed-responsive-16by9">
                  <?= $vid_link ?>
                </div>
              <?php } ?>

            </div><!-- title-ex-fevid -->
          </div>
            
          <?php endwhile; ?>
          </div>
          <div class="pagination-links">
          <?php
           $big = 999999999;
           echo paginate_links( array(
              'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
              'format' => '?paged=%#%',
              'current' => max( 1, get_query_var('paged') ),
              'total' => $querys->max_num_pages
           ) );
          ?>
        </div>
        <?php endif;
        wp_reset_postdata();
        endif; ?>


        <?php if( $template == "dlibraries" ) :
          get_template_part("templates/dom-libraries-acc");
        endif; ?>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>
<?php endif;

if( $template == "tablebig" ) : 
  get_template_part("templates/table-big");
endif; ?>

<?php get_footer(); ?>