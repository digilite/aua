<?php get_header();?>

<main class="projects-page ftm">
  <div class="container">
    <div id="plug-breadcrumbs">
      <?php if (function_exists('show_full_breadcrumb')) show_full_breadcrumb(); ?>
    </div>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php the_title(); ?></h1>
        <section class="spacing">

        <?php
        if (have_posts()) :
	      while (have_posts()) : the_post(); ?>
            <div class="row">
    
                <div class="col-12">
                <div class="col-5 float-right talk-single-img">
                    <?php if( has_post_thumbnail() ) { 
                        the_post_thumbnail();
                     } ?>
                </div>
                    <?php the_content(); ?>
                </div>

            </div><!-- row -->
        <?php endwhile; endif; ?>
      </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>
