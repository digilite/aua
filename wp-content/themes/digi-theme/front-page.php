<?php get_header(); ?>

<div class="container">
  <div class="row home-hero">
    <div class="col-lg-8">
      <?php get_template_part("templates/hero-function-areas"); ?>
    </div>

    <div class="col-lg-4">
      <?php get_template_part("templates/hero-sidebar"); ?>
    </div>
  </div><!-- row -->
</div>

<section class="talks-projects">
  <?php get_template_part("templates/home-talks-proj"); ?>
</section>


<section class="collabs">
  <?php get_template_part("templates/home-collabs"); ?>
</section>


<section class="zones">
  <div class="container">
    <?php get_template_part("templates/home-zones"); ?>
  </div><!-- Container -->
</section>

<section class="highlights-slider">
  <?php get_template_part("templates/highlights-slider"); ?>
</section>

<?php get_footer(); ?>
