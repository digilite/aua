		<footer itemscope itemtype="http://schema.org/WPFooter">
      <div id="footer-menus" class="container">
        <div class="row">
          <div class="col-md-2 menu-with-social">
            <?php
            wp_nav_menu([
              'theme_location' => 'footer-menu-1',
              'menu_class' => 'footer-menu-1',
              'container' => '',
            ]); ?>

            <div class="social-links">
            <?php get_template_part( 'social' ); ?>
            </div>
          </div>

          <div class="col-md-2 rand">
            <?php
            wp_nav_menu([
              'theme_location' => 'footer-menu-2',
              'menu_class' => 'footer-menu-1',
              'container' => '',
            ]); ?>
          </div>

          <div class="col-md-2">
            <?php
            wp_nav_menu([
              'theme_location' => 'footer-menu-3',
              'menu_class' => 'footer-menu-1',
              'container' => '',
            ]); ?>
          </div>

          <div class="col-md-4 offset-md-2">
            <div class="footer-form">
              <p><?= __("Write to us","digi-theme"); ?></p>
              <?php echo do_shortcode( '[gravityform id="16" title="false" description="false"]' ); ?>
            </div>
          </div>
        </div><!-- row -->
      </div><!-- Footer Menus -->

      <div class="footer-description">
        <div class="container">
          <p><?= get_field("footer_description_text", "option"); ?></p>
        </div>
      </div>

      <div id="copyright">
        &copy; <?php echo date("Y")?> <?= __("American University of Armenia","digi-theme"); ?>
      </div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>
