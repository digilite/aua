<?php
/*
* Template Name: Sub Default template
*/

get_header();?>

<main class="collaborations-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php the_title(); ?></h1>
        <section class="subi-page">
          <?php
          if (have_posts()) :
            while (have_posts()) :
              the_post(); ?>
              <?php the_content(); ?><?php
            endwhile;
          endif; ?>
        </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>






<?php get_footer(); ?>
