<?php
/*
* Template Name: People
*/

get_header();?>

<main class="team-page">
  <div class="container">

  <?php custom_breadcrumbs(); ?>
  <h1><?php the_title(); ?></h1>
    <?php switch_to_blog(121); ?><?php echo do_shortcode("[team id='224']"); ?><?php restore_current_blog(); ?>
  </div><!-- Container -->
</main>

<?php get_footer(); ?>