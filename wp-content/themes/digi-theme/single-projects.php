<?php get_header();?>

<main class="projects-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php the_title(); ?></h1>
        <section class="spacing">

        <?php
        if (have_posts()) :
	      while (have_posts()) : the_post(); ?>
            <div>
              <div class="proj-single-img">
                  <?php if( has_post_thumbnail() ) { 
                      the_post_thumbnail();
                   } ?>
              </div>
              <div>
                  <?php the_content(); ?>
              </div>
            </div>

            <?php if( have_rows('accordion') ): ?>
                
                <?php while( have_rows('accordion') ): the_row(); 
                    $title = get_sub_field('accordion_title');
                    $main_phone = get_sub_field('accordion_title');
                    $clear_phone = str_replace(' ', '', $main_phone);
                    $part_phone = str_replace('|', '', $clear_phone);
                    $part2_phone = str_replace('.', '', $part_phone);
                    $final_title = str_replace('-', '', $part2_phone);
                    $content = get_sub_field('accordion_content');
                    ?>
                    <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="heading<?= $final_title ?>">
                        <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $final_title ?>" aria-expanded="false" aria-controls="collapse<?= $final_title ?>">
                            <?= $title ?>
                        </button>
                        </h2>
                        </div>

                        <div id="collapse<?= $final_title ?>" class="collapse" aria-labelledby="heading<?= $final_title ?>" data-parent="#accordionExample">
                        <div class="card-body">
                            <?= $content ?>
                        </div>
                    </div><!-- Card -->
                    </div>
                <?php endwhile; ?>
                
            <?php endif; ?>


        <?php endwhile; endif; ?>
      </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>
