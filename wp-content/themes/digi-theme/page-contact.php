<?php
/*
* Template Name: Contact Us
*/
$contatcs_desc = get_field("contacts_page_description", "options");
$phone_1 = get_field("phone_1", "options");
$phone_2 = get_field("phone_2", "options");
$phone_3 = get_field("phone_3", "options");
$email = get_field("email", "options");

$main_phone = get_field('phone_1', 'option');
$clear_phone = str_replace(' ', '', $main_phone);
$part_phone = str_replace(')', '', $clear_phone);
$part2_phone = str_replace('(', '', $part_phone);
$final_phone_1 = str_replace('-', '', $part2_phone);

$main_phone_2 = get_field('phone_2', 'option');
$clear_phone_2 = str_replace(' ', '', $main_phone_2);
$part_phone_2 = str_replace(')', '', $clear_phone_2);
$part2_phone_2 = str_replace('(', '', $part_phone_2);
$final_phone_2 = str_replace('-', '', $part2_phone_2);

$main_phone_3 = get_field('phone_3', 'option');
$clear_phone_3 = str_replace(' ', '', $main_phone_3);
$part_phone_3 = str_replace(')', '', $clear_phone_3);
$part2_phone_3 = str_replace('(', '', $part_phone_3);
$final_phone_3 = str_replace('-', '', $part2_phone_3);

$phone_title = __("Phone","digi-theme");
$email_title = __("Email","digi-theme");
$write_to_us = __("Write to us","digi-theme");

get_header(); ?>

<main class="contact-us">
  <div class="container contact-info">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-4">
        <h1><?php the_title(); ?></h1>
        <p>
        <?= $contatcs_desc ?>
        </p>
        <div class="phone-info d-flex">
          <span><?= $phone_title ?>:</span>
          <div class="phone-nums d-flex flex-column">
            <a href="tel:<?= $final_phone_1 ?>"><?= $phone_1 ?></a>
            <a href="tel:<?= $final_phone_2 ?>"><?= $phone_2 ?></a>
            <a href="tel:<?= $final_phone_3 ?>"><?= $phone_3 ?></a>
          </div>
        </div><!-- phone-info -->
        <div class="email-info d-flex">
          <span><?= $email_title ?>:</span>
          <a href="mailto:<?= $email ?>"><?= $email ?></a>
        </div>
      </div>
      <div class="col-md-6 offset-md-2">
        <div class="contact-us-from">
          <p><?= $write_to_us ?></p>
          <?php the_content(); ?>
        </div>
      </div>
    </div><!-- Row -->
  </div><!-- Container -->
  <div class="contact-us-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3047.7193106300883!2d44.50225561564411!3d40.193059677135615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x406abd173fd4d621%3A0x213beef7d45c07c5!2sAmerican%20University%20of%20Armenia!5e0!3m2!1sen!2s!4v1643197770559!5m2!1sen!2s" width="100%" height="230" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
  </div>
</main>

<?php get_footer(); ?>
