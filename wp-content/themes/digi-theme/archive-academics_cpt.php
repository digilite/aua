<?php get_header(); 
$academics_title = __("Enviornment-related Academics","digi-theme");
?>

<main class="collaborations-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?= $academics_title ?></h1>
        <div class="icon-text-container">
        <?php 
        $terms = get_terms([
          'taxonomy' => 'academics_cats',
          'hide_empty' => false,
          'parent' => 0
        ]);
        foreach ($terms as $term): 
          $image = get_field('category_thumbnail', $term);
          $description = get_term_field( 'description', $term );
          ?>
        
          <div class="icon-text-box">
            <a href="<?= get_term_link($term); ?>" class="icon-text-box-link"></a>
            <div class="icon-text-icon">
              <img src="<?= $image ?>" alt="<?= $term->name; ?>">
            </div>
            <div class="icon-text-box-info">
              <h5><?= $term->name; ?></h5>
              <p><?= $description ?></p>
            </div>
          </div><!-- icon-text-box -->
        <?php endforeach; ?>
        </div><!-- icon-text-container -->

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>


<?php get_footer(); ?>