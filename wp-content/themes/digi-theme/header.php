<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="//fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<?php wp_head(); ?>
</head>
<body id="<?php if ( ICL_LANGUAGE_CODE !== "en") echo'arm'; ?>" <?php body_class(); ?>>
	<header itemscope itemtype="http://schema.org/WPHeader">
    <div id="topbar">
      <div class="container">
        <div class="topbar-menu">
          <?php
          wp_nav_menu([
            'theme_location' => 'topbar-menu',
            'menu_class' => '',
            'container' => '',
          ]); ?>
        </div>
      </div>
    </div>
		<div class="container flex-container align-items-center logo-menu">
			<div itemscope itemtype="http://schema.org/Organization" id="logo">
				<a itemprop="url" href="<?php echo bloginfo('url') ?>">
        <?php if ( ICL_LANGUAGE_CODE == "en") { ?>
          <img itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/AUA_Logo-En.svg" alt="AUA Logo">
        <?php } else { ?>
          <img itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/main-logo-Arm.svg" alt="AUA Logo">
        <?php } ?>	
				</a>
			</div>
			<nav class="navbar-expand-lg" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="burger-one"></span>
					<span class="burger-two"></span>
					<span class="burger-three"></span>
				</button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <?php
          wp_nav_menu([
            'theme_location' => 'primary-menu',
            'menu_class' => 'main-menu navbar-nav',
            'container' => '',
          ]);
          wp_nav_menu([
            'theme_location' => 'mobile-menu',
            'menu_class' => 'main-menu-mobile navbar-nav',
            'container' => '',
          ]); 
          ?>
        </div>
			</nav>

      <div class="search">
        <?php get_search_form(); ?>
      </div>
		</div>
	</header>
