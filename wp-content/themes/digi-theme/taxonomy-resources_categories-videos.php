<?php get_header(); ?>

<main class="collaborations-page ftm">
  <div class="container">
    <ul id="breadcrumbs" class="breadcrumbs">
      <li class="item-home">
        <a class="bread-link bread-home" href="<?php bloginfo( 'url' ) ?>" title="Home">Home</a>
      </li>
      <li class="separator separator-home"> &gt; </li>
      <li class="item-cat item-custom-post-type-"><a class="bread-cat bread-custom-post-type-" href="<?php echo get_post_type_archive_link( 'resources_cpt' ); ?>" title="">Resources</a></li>
      <li class="separator"> &gt; </li>
      <li class="item-current item-archive"><strong class="bread-current bread-archive">Videos</strong></li>
    </ul>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php single_term_title(); ?></h1>

        <div id="youtube-videos">
        <?php echo do_shortcode( '[gs_ytgal theme="gs_ytgal_grid"]' ); ?>
        <div class="morebutton"><a href="https://www.youtube.com/channel/UCB0RsWxilKom4kzbxppwRhQ" target="_blank"><?= __("View All","digi-theme") ?></a></div>
        </div>
      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>