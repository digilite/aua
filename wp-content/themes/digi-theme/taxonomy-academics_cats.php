<?php get_header(); ?>

<main class="collaborations-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php single_term_title(); ?></h1>

          <?php
          $tax_name = 'academics_cats';
          $the_id = get_queried_object()->term_id;
          $terms = get_terms([
            'taxonomy' => $tax_name,
            'hide_empty' => false,
            'parent' => $the_id
          ]); ?>

            <section class="title-with-links">
            <?php foreach ($terms as $term): ?>
      
              <div class="single-collab">
              <h3 class="h5"><?= $term->name; ?></h3>
                  
              <?php
              $args = array(
                  'post_type' => 'academics_cpt',
                  'posts_per_page' => -1,
                  'tax_query' => array(
                      array(
                      'taxonomy' => 'academics_cats',
                      'field'    => 'slug',
                      'terms'    => $term->slug,
                      ),
                  ),
              );

              $_posts = new WP_Query( $args );
              if( $_posts->have_posts() ) : ?>
              <ul>
              <?php while( $_posts->have_posts() ) : $_posts->the_post(); ?>
                  <li>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  </li>
              <?php endwhile; ?>
              </ul>
              <?php endif; wp_reset_postdata(); ?>
              </div><!-- Single Colllab -->
			      <?php endforeach; ?>
            </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>