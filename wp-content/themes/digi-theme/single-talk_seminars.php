<?php get_header();?>

<main class="projects-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php the_title(); ?></h1>

        <section class="spacing">

        <?php
        if (have_posts()) :
        while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-md-8">
                    <?php the_content(); ?>
                </div>
                <div class="col-md-4 talk-single-img">
                    <?php if( has_post_thumbnail() ) { 
                        the_post_thumbnail();
                     } ?>
                </div>
            </div><!-- row -->
        <?php endwhile;
	    endif; ?>

      </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>
