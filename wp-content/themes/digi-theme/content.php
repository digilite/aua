<?php
if( have_rows("function_areas_content") ):
    // loop through the rows of data
    while ( have_rows("function_areas_content") ) : the_row();
        switch (get_row_layout()) :
            case "initiatives_and_projects":
                get_template_part("templates/projects-acc");
                break;
            case "people":
                get_template_part("templates/team-acc");
                break;
            case "title_with_links":
                get_template_part("templates/title-links-acc");
                break;
        endswitch;
    endwhile;
endif;
?>