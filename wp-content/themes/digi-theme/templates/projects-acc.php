<?php 
$section_title = get_sub_field("section_title");
$project_type = get_sub_field("choose_project_type");
?>

    <div class="card">
    <div class="card-header" id="headingOne">
    <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <?= $section_title ?>
        </button>
    </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
    <div class="card-body">
        <?php
        $args = array(
            'post_type' => 'projects',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                   'taxonomy' => 'post_tag',
                   'field'    => 'term_id',
                   'terms'    => $project_type,
                ),
                array(
                    'taxonomy' => 'project_status',
                    'field'    => 'slug',
                    'terms'    => 'ongoing',
                   )
                ),
        );

        $query = new WP_Query($args);

        if ($query->have_posts()) : ?>
            <section class="projects-page">
            <h3>Ongoing</h3>
            <div class="colored-boxes d-flex">
        <?php
            while ($query->have_posts()) {
            $query->the_post();
            get_template_part( 'templates/colored-box' ); 
            } ?>
        
        </div>
        </section>
        <?php endif; wp_reset_postdata();

        $args = array(
            'post_type' => 'projects',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                   'taxonomy' => 'post_tag',
                   'field'    => 'term_id',
                   'terms'    => $project_type,
                ),
                array(
                    'taxonomy' => 'project_status',
                    'field'    => 'slug',
                    'terms'    => 'completed',
                   )
                ),
        );

        $query = new WP_Query($args);

        if ($query->have_posts()) : ?>
        <section class="projects-page">
        <h3>Completed</h3>
        <div class="colored-boxes d-flex">
        <?php
            while ($query->have_posts()) {
            $query->the_post();
            get_template_part( 'templates/colored-box' ); 
        } ?>
        
        </div>
        </section>
        <?php endif; wp_reset_postdata(); ?>
    </div>
    </div>
    </div><!-- Card -->
