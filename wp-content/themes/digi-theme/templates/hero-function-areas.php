<?php
$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => 15039,
    'orderby'        => 'menu_order',
    'order'          => 'ASC'
 );


$parent = new WP_Query( $args ); ?>

<h1 class="hero-heading"><?= __("Function Areas","digi-theme") ?></h1>
<div class="masno">
<?php
if ( $parent->have_posts() ) : ?>
    <?php while ( $parent->have_posts() ) : $parent->the_post(); 
    $short_desrption = get_field("short_desrption"); ?>
        
        <div class="flip-card">
            <div class="flip-card-inner">
                <div class="flip-card-front" style="background-image: linear-gradient(0deg, rgba(0, 59, 92, 0.8) 18.82%, rgba(255, 255, 255, 0) 98.08%) ,url(<?php the_post_thumbnail_url(); ?>);">
                    <span><?php the_title(); ?></span>
                </div>
                <div class="flip-card-back">
                    <a href="<?php the_permalink(); ?>"></a>
                    <p><?= $short_desrption ?></p>
                    <span><a id="backlink" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
                </div>
            </div>
        </div>

    <?php endwhile;
endif; wp_reset_postdata(); ?>
</div>