<?php
$argsh = array(
    'post_type' => 'highlights',
    'posts_per_page' => -1,
    );

$queryh = new WP_Query($argsh); ?>

<h2><?= __("2020/2021 HIGHLIGHTS","digi-theme") ?></h2>
<div class="fixed-back">
<div class="container relative-parent">
  <div class="owl-carousel owl-theme" id="highlights-carousel">
    <?php
    if ($queryh->have_posts()) : ?>
    <?php while ($queryh->have_posts()) : $queryh->the_post(); 
      $high_number = get_field("number"); ?>
        <div class="item">
            <div class="high-icon">
              <?php the_post_thumbnail(); ?>
            </div>
            <div class="high-desc">
              <span><?= $high_number ?></span>
              <p><?php the_title(); ?></p>
            </div>
        </div><!-- item -->
      <?php endwhile; ?>
	<?php endif; wp_reset_postdata();?>
  </div>
  <div class="slider-button">
    <a href="<?= get_post_type_archive_link( 'highlights' ); ?>" class="button"><?= __("Show all","digi-theme") ?></a>
  </div>
</div>
</div>