<?php
$tax_name = 'resources_categories';
$the_id = get_queried_object()->term_id;
$termf = get_queried_object();

$terms = get_terms([
'taxonomy' => $tax_name,
'hide_empty' => false,
'parent' => $the_id,
]);

foreach ($terms as $term) : ?>         

<div class="accordion" id="accordionlib">
    <div class="card">
        <div class="card-header" id="heading<?= $term->name; ?>">
        <h2 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $term->name; ?>" aria-expanded="false" aria-controls="collapseOne">
            <?= $term->name; ?>
            </button>
        </h2>
    </div>

    <div id="collapse<?= $term->name; ?>" class="collapse" aria-labelledby="heading<?= $term->name; ?>" data-parent="#accordionlib">
    <div class="card-body">
    <?php
    $argsl = array(
        'post_type' => 'resources_cpt',
        'posts_per_page' => -1,
        's' => $search_text,
        'tax_query' => array(
        'relation' => 'AND',
            array(
            'taxonomy' => $tax_name,
            'field'    => 'slug',
            'terms'    => $term->slug,
            ),
            array(
            'taxonomy' => 'resources_tag',
            'field'    => 'slug',
            'terms'    => $topic,
            'operator' => 'AND',
            ),
        )
    );
    $queryl = new WP_Query($argsl);

    if ($queryl->have_posts()) : 
        $count = 1;
        get_template_part("templates/filters"); ?>

        <table class="resource-table">
        <tr class="table-titles">
            <th>#</th>
            <th>Title</th>
            <th>No</th>
            <th>Date</th>
        </tr>
        <?php
        while ($queryl->have_posts()) : $queryl->the_post();  
        $posttags = get_the_terms( get_the_ID(), 'resources_tag' );
        $no_num = get_field("no_num");
        ?>
        
        <tr>
            <td><?= $count ?></td>
            <td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
            <td>
            <?php if($no_num) :
                echo '-'.$no_num.'-';
            endif; ?>
            </td>
            <td class="lang"><?= get_the_date( 'Y' ); ?></td>
        </tr>
        <?php $count++; ?>
        <?php endwhile; ?>
        </table>
    <?php endif; wp_reset_postdata(); ?>
    
    </div>
    </div>
    </div><!-- Card -->
</div><!-- accordion -->

<?php endforeach; ?>