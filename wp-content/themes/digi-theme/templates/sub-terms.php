<div class="sub-cats-container">
<?php
$tax_name = 'resources_categories';
$the_id = get_queried_object()->term_id;
$parent_id = wp_get_term_taxonomy_parent_id( $the_id, $tax_name );

if ($parent_id == 0) {
    $id = get_queried_object()->term_id;
} else {
    $id = wp_get_term_taxonomy_parent_id( $the_id, $tax_name );
}

$terms = get_terms([
    'taxonomy' => $tax_name,
    'hide_empty' => false,
    'parent' => $id,
]);


foreach ($terms as $term) :
    $image = get_field('category_thumbnail', $term); ?>
             
    <div class="sub-cat-box <?php if ( $the_id == $term->term_id ) echo "active-sub"; ?>">
        <a href="<?= get_term_link($term); ?>" class="icon-text-box-link"></a>
        <div class="sub-cat-icon">
            <img src="<?= $image ?>" alt="<?= $term->name; ?>">
        </div>
        <div class="sub-cat-title">
            <h5><?= $term->name; ?></h5>
        </div>
    </div><!-- sub-cat-box -->
<?php endforeach; ?>
</div><!-- sub-cats-container -->