<?php
$tax_name = 'resources_categories';
$the_id = get_queried_object()->term_id;
$termf = get_queried_object();
$parent_id = wp_get_term_taxonomy_parent_id( $the_id, $tax_name );
$template = get_field( 'choose_template', $termf );
$child_term = get_term( $the_id, $tax_name );
$parent_term = get_term( $child_term->parent, $tax_name );
$current_term = single_term_title( "", false );

$smth = get_term( $the_id, $tax_name );
$slug = $smth->slug;

if($_GET['topic'] && !empty($_GET['topic'])) {
  $topic = $_GET['topic'];
}

if($_GET['search_text'] && !empty($_GET['search_text'])) {
  $search_text = $_GET['search_text'];
}

$args = array(
  'post_type' => 'resources_cpt',
  'posts_per_page' => -1,
  's' => $search_text,
  'tax_query' => array(
      array(
         'taxonomy' => $tax_name,
         'field'    => 'slug',
         'terms'    => $slug,
      ),
      array(
        'taxonomy' => 'post_tag',
        'field'    => 'slug',
        'terms'    => $topic,
        'operator' => 'AND',
     ),
    )
);
$querybig = new WP_Query($args); ?>

<main class="collaborations-page ftm">
  <div class="container">
    <div id="plug-breadcrumbs">
      <?php if (function_exists('show_full_breadcrumb')) show_full_breadcrumb(); ?>
    </div>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1>
          <?php if( !empty($parent_term->name) ) {
              echo $parent_term->name;
            } else {
              echo $current_term;
            } ?>
        </h1>

        <?php if( $template == "tablebig" ) : 
          get_template_part("templates/sub-terms"); 
        endif; 
        
        get_template_part("templates/filters"); ?>

        <?php 
        if ($querybig->have_posts()) { ?>
          <table class="resource-table">
          <tr class="table-titles">
            <th>Title</th>
            <th>Author(s)</th>
            <th>Publisher</th>
            <th>Year</th>
            <th>Languages</th>
            <th>Keyword</th>
          </tr>
        <?php
          while ($querybig->have_posts()) :
          $querybig->the_post();  
          $posttags = get_the_terms( get_the_ID(), 'post_tag' );
          $authors = get_field("authors");
          $publisher = get_field("publisher");
          $language = get_field("pub_language");
          ?>
            <tr>
              <td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
              <td><?= $authors ?></td>
              <td><?= $publisher ?></td>
              <td><?= get_the_date( 'Y' ); ?></td>
              <td class="lang"><?= $language ?></td>
              <td class="tags">
              <?php
              foreach( $posttags as $tag ) { 
               echo $tag->name.'<span>, </span>';
              } ?>
              </td>
            </tr>
          
          <?php endwhile; ?>
          </table>
        <?php }   else {
         echo "<p class='no-data-found'>No data found.</p>"; 
        } wp_reset_postdata(); ?>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>