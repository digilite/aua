<?php 
$res_title = __("Enviornment-related Resources","digi-theme");
$academics_title = __("Enviornment-related Academics","digi-theme");

$termsres = get_terms([
  'taxonomy' => 'resources_categories',
  'hide_empty' => false,
  'parent' => 0
]);

$termsaca = get_terms([
  'taxonomy' => 'academics_cats',
  'hide_empty' => false,
  'parent' => 0
]);

if ( ICL_LANGUAGE_CODE == "en") {
    $trid = 15225;
} else {
    $trid = 15849;
} ?>

<div class="row">
    <div class="col-md-4">
        <div class="single-zone">
        <?php $args = array('post_parent' => 15225, 'post_type' => 'page', 'numberposts' => -1, 'post_status' => 'publish', 'order' => 'DESC'); 
        $parents = get_children($args); ?>
            <h3 class="h1"><a href="<?= get_page_link( 15225 ) ?>"><?= get_the_title( $trid ); ?></a></h3>  
            <ul>
            <?php foreach ($parents as $parent) : ?>
                <li><a href="<?= get_permalink($parent); ?>"><?= $parent->post_title; ?></a></li>
            <?php endforeach; ?>
            </ul>
        </div><!-- Single Colllab -->
    </div>

    <div class="col-md-4">
        <div class="single-zone">
            <h3 class="h1"><a href="<?= get_post_type_archive_link( 'resources_cpt' ); ?>"><?= $res_title ?></a></h3>
            <ul>
            <?php foreach ($termsres as $term) : ?>
                <li><a href="<?= get_term_link($term); ?>"><?= $term->name; ?></a></li>
            <?php endforeach; ?>
            </ul>
        </div><!-- Single Colllab -->
    </div>

    <div class="col-md-4">
        <div class="single-zone">
            <h3 class="h1"><a href="<?= get_post_type_archive_link( 'academics_cpt' ); ?>"><?= $academics_title ?></a></h4>
            <ul>
            <?php foreach ($termsaca as $term):  ?>
                <li><a href="<?= get_term_link($term); ?>"><?= $term->name; ?></a></li>
            <?php endforeach; ?>
            </ul>
        </div><!-- Single Colllab -->
    </div>
</div><!-- Row -->