<?php 
    $section_title = get_sub_field("section_title");
    $key=0;
    $add_people = [];
    $people_ids = [];

    if( have_rows('add_people') ): while( have_rows('add_people') ): the_row(); 
        $add_people[$key]["ID"] = get_sub_field('the_member');
        $add_people[$key]["position"] = get_sub_field('member_titile');
        $people_ids [$key] = $add_people[$key]["ID"];
        $key++;
    endwhile; endif;
    
    $args = array(
        'post_type' => 'team_member',
        'post__in' => $people_ids,
        'order'   => 'ASC',
        'orderby' => 'post__in',
    );
?>


    <div class="card">
        <div class="card-header" id="headingOne">
        <h2 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsePeople" aria-expanded="false" aria-controls="collapseTeam">
                <?= $section_title ?>
            </button>
        </h2>
        </div>

        <div id="collapsePeople" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body">

        <?php
        switch_to_blog(121); 
        $query = new WP_Query($args); 
        $count = 0;
        if ($query->have_posts()): ?>
        <div class="row">
        <?php while ($query->have_posts()): $query->the_post(); ?>
            <div class="col-lg-4 col-md-6 col-6">
                <div class="team-member">
                    <a class="acc-member-img" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                    <div class="member-info">
                    <a href="<?php the_permalink(); ?>"><p><?php the_title(); ?></p></a>
                    <span><?= $add_people[$count]["position"]; ?></span>
                    </div>
                </div>
            </div><!-- Col -->
        <?php $count++;
        endwhile; ?>
        </div><!-- row -->
        <?php endif; restore_current_blog(); wp_reset_postdata(); ?>
        </div>
        </div>
    </div><!-- Card -->
