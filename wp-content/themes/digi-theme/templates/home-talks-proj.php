<?php
$talk_sems_title = __("Talks and Seminars","digi-theme");
$projects_title = __("Projects","digi-theme");
?>

<div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2><?= $talk_sems_title ?></h2>
        <div class="colored-boxes d-flex">
            <?php 
            $args = array(
                'post_type' => 'talk_seminars',
                'posts_per_page' => 3,
                'order' => 'DESC',
            );
            
            $my_query = new WP_Query($args);
            while ($my_query->have_posts()) : $my_query->the_post();
            $date = get_field("date"); ?>
                <div id="talk-sems" class="colored-box">
                    <div class="colored-box-title">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                    </div>
                    <p>
                        <time class="talks-sems-date"><?= $date ?></time>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class=fptitle>
                        <?php the_title(); ?></a>
                    </p>
                </div><!-- Colored Box -->
            <?php endwhile; wp_reset_postdata(); ?>

        </div><!-- Colored boxes -->
        <a href="<?= get_post_type_archive_link( 'talk_seminars' ); ?>" class="button white-button"><?= __("show all","digi-theme") ?></a>
      </div>

      <div class="col-md-6">
      <h2><?= $projects_title ?></h2>
        <div class="colored-boxes d-flex">
            <?php 
            $args = array(
                'post_type' => 'projects',
                'posts_per_page' => 3,
                'order' => 'DESC',
            );
            $my_query = new WP_Query($args);
            while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <div class="colored-box">
                    <div class="colored-box-title">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                    </div>
                    <p><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="fptitle">
                    <?php the_title(); ?></a></p>
                </div><!-- Colored Box -->
            <?php endwhile; wp_reset_postdata(); ?>

        </div><!-- Colored boxes -->
        <a href="<?= get_post_type_archive_link( 'projects' ); ?>" class="button white-button"><?= __("show all","digi-theme") ?></a>
      </div>
    </div><!-- Row -->
  </div><!-- container -->