<?php
  if($_GET['topic'] && !empty($_GET['topic'])) {
    $topic = str_replace('-',' ',$_GET['topic']);

  } else {
    $topic = "Click to select";
  }
  if($_GET['search_text'] && !empty($_GET['search_text'])) {
      $search = $_GET['search_text'];
  }
?>


<form method="get" id="posts-filter">
  <label>Keyword:</label>
  <select name="topic">
    <option value=""><?= __($topic,"digi-theme") ?></option>

    <?php
    
    $filtertags = get_tags(array(
      'taxonomy' => 'post_tag',
      'hide_empty' => true
    ));
   
    foreach( $filtertags as $tag ) : ?>
      <option value="<?= $tag->slug ?>"><?= $tag->name ?></option>
    <?php endforeach; ?>
  </select>
  <button class="extra-filter" type="submit" name="">Filter</button>

  <div class="search-field">
    <input id="asds" type="text" name="search_text" onfocus="this.value=''" value="<?= $search; ?>">
    <button type="submit" name=""><i class="fas fa-search"></i></button>
  </div>
  <input type="reset" value="Reset" class="reset">
</form> 