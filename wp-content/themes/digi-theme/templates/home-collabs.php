<?php $collabs_title = __("Acopian Center Collaborations","digi-theme"); ?>

<div class="container">
  <h2><?= $collabs_title ?></h2>

  <?php
  if ( ICL_LANGUAGE_CODE == "en") {
    if( have_rows('collaborations', 15101) ): ?>
      <div class="row">
      <?php while( have_rows('collaborations', 15101) ): the_row(); 
        $single_collab_title = get_sub_field('single_collaboration_title');
        ?>
        <div class="col-lg-4 col-md-12">
          <div class="single-collab">
          <h3 class="h5"><?= $single_collab_title ?></h3>
            <!-- Inner repeater start -->
            <?php if( have_rows('single_collaboration_links') ): ?>
            <ul id="collab-list">
              <?php while( have_rows('single_collaboration_links') ): the_row(); 
              $text = get_sub_field('text'); 
              $text_url = get_sub_field('text_url'); ?>
              <li>
                <a href="<?= esc_url($text_url); ?>" target="_blank"><?= $text ?></a>
              </li>
              <?php endwhile; ?>
            </ul>
            <?php endif; ?>
            <!-- Inner repeater end -->
          </div><!-- Single Colllab -->
        </div>
        <?php endwhile; ?>
      </div><!-- Row -->
    <?php endif;
  } else {
    if( have_rows('collaborations', 15917) ): ?>
      <div class="row">
      <?php while( have_rows('collaborations', 15917) ): the_row(); 
        $single_collab_title = get_sub_field('single_collaboration_title');
        ?>
        <div class="col-lg-4 col-md-12">
          <div class="single-collab">
          <h3 class="h5"><?= $single_collab_title ?></h3>
            <!-- Inner repeater start -->
            <?php if( have_rows('single_collaboration_links') ): ?>
            <ul id="collab-list">
              <?php while( have_rows('single_collaboration_links') ): the_row(); 
              $text = get_sub_field('text'); 
              $text_url = get_sub_field('text_url'); ?>
              <li>
                <a href="<?= esc_url($text_url); ?>" target="_blank"><?= $text ?></a>
              </li>
              <?php endwhile; ?>
            </ul>
            <?php endif; ?>
            <!-- Inner repeater end -->
          </div><!-- Single Colllab -->
        </div>
        <?php endwhile; ?>
      </div><!-- Row -->
    <?php endif;
  } ?>

    <div class="collabs-buttons d-flex flex-column">
      <a href="<?php echo esc_url( get_page_link( 15101 ) ); ?>" class="button white-button"><?= __("Show All","digi-theme") ?></a>
      <a href="#" class="collab-button-aua button blue-button"><?= __("AUA collaborations","digi-theme") ?></a>
    </div>
  </div><!-- Container -->