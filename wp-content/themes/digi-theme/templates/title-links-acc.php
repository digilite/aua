<?php
$section_title = get_sub_field("section_title");
$add_smth = get_sub_field("add_button_or_icon");

$main_phone = get_sub_field("section_title");
$clear_phone = str_replace(' ', '', $main_phone);
$part_phone = str_replace('|', '', $clear_phone);
$part2_phone = str_replace('.', '', $part_phone);
$final_title = str_replace('-', '', $part2_phone);
?>

<div class="accordion" id="accordion<?= $final_title ?>">
    <div class="card">
    <div class="card-header" id="heading<?= $final_title ?>">
    <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse<?= $final_title ?>" aria-expanded="false" aria-controls="collapse<?= $final_title ?>">
        <?= $section_title ?>
        </button>
    </h2>
    </div>

    <div id="collapse<?= $final_title ?>" class="collapse" aria-labelledby="heading<?= $final_title ?>" data-parent="#accordionExample">
    <div class="card-body <?php if ($add_smth == 'withbutton') echo 'add-bt'; ?><?php if ($add_smth == 'withicon') echo 'add-icon'; ?>">
        
    <?php if( have_rows('single_title_with_links') ): ?>
        <section class="title-with-links">
        <?php while( have_rows('single_title_with_links') ): the_row(); 
            $single_section_title = get_sub_field('single_section_title');
            $the_icon = get_sub_field("text_links_icon");
            $btn_url = get_sub_field("show_all_button_link");
            ?>
            <div class="single-collab">
            <div class="title-links-icon">
            <?php if( $add_smth == 'withicon' ) { ?>
                <img src="<?= $the_icon ?>" alt="icon">
            <?php } ?>
            </div>
            <h3 class="h5"><?= $single_section_title ?></h3>
            <!-- Inner repeater start -->
            <?php if( have_rows('single_section_links') ): ?>
                <ul>
                <?php while( have_rows('single_section_links') ): the_row(); 
                    $text = get_sub_field('text'); 
                    $text_url = get_sub_field('text_url'); ?>
                    <li>
                        <a href="<?= esc_url($text_url) ?>"><?= $text ?></a>
                    </li>
                <?php endwhile; ?>
                </ul>
                <?php if( $add_smth == 'withbutton' ) : ?>
                    <a href="<?= esc_url($btn_url); ?>" class="button white-button">show all</a>
                <?php endif; ?>
            <?php endif; ?>
            <!-- Inner repeater end -->
            </div><!-- Single Colllab -->
        <?php endwhile; ?>
        </section>
    <?php endif; wp_reset_postdata(); ?>

    </div>
    </div>
    </div><!-- Card -->
</div>