<?php
if ( ICL_LANGUAGE_CODE == "en") {
    $news_id = 177;
    $latest_dev = 709;
} else {
    $news_id = 178;
    $latest_dev = 710;
} ?>


<div class="tabs effect-1">

    <!-- tab-content -->
    <div class="tab-content">
        <section id="tab-item-3">
            <span><?= __("In Focus","digi-theme") ?></span>
            <?php
            $args = array(
            'post_type' => 'post',
            'category'=> $latest_dev,
            'numberposts' => '6',
            'suppress_filters' => 0,
            );
            $latdev = get_posts($args);
 
        if(count($latdev) == 0): ?>
            <p style="margin-top: 10px;"><?= __("Check back later","digi-theme") ?></p>
        <?php else: ?>
            <div id="latest-devs" class="owl-carousel owl-theme">
                <?php foreach ( $latdev as $post): ?>
                    <div class="sidebar-slider">
                        <div class="limited-content">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                        </div>
                        <p><a href="<?php the_permalink(); ?>"><?= $post->post_title; ?></a></p>
                    </div><!-- Colored Box -->
                <?php endforeach; ?>
            </div><!-- CAROUSEL -->
        <?php endif; wp_reset_postdata(); ?>
        </section>

        <section id="tab-item-1">
        <span><?= __("News","digi-theme") ?></span>
        <?php switch_to_blog(35); 
        $args = array(
            'post_type' => 'post',
            'category'=> $news_id,
            'numberposts' => '5',
            'suppress_filters' => 0,
        );
        $news = get_posts($args);
 
        if(count($news) == 0): ?>
            <p style="margin-top: 10px;"><?= __("Check back later for news","digi-theme") ?></p>
            <div class=morebutton><a href="//newsroom.aua.am/category/science/environment/" target="_blank"><?= __("View All","digi-theme") ?></a></div>
        <?php else: ?>
            <ul>      
                <?php foreach ( $news as $post): ?>
                    <li><a href="<?= $post->guid ?>" title="<?= $post->post_title; ?>"><?= $post->post_title;; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <br>
            <div class="morebutton"><a href="//newsroom.aua.am/category/science/environment/" target="_blank"><?= __("View All","digi-theme") ?></a></div>
        <?php endif; restore_current_blog(); ?>
        </section>

        <section id="tab-item-2">
        <span><?= __("Public Events","digi-theme") ?></span>
         <?php
         switch_to_blog(35); 
            $args = array(
                'post_type' => 'tribe_events',
                'tribe_events_cat'=> 'environment',
                'numberposts' => '5',
                'orderby' => 'date',
                'order' => 'DESC',
                'suppress_filters' => 0
            );

        $events = get_posts($args);
        if(count($events) == 0): ?>
            <p style="margin-top: 10px;">Check back later for events</p>
            <div class=morebutton><a href="//newsroom.aua.am/events/category/environment/" target="_blank"><?= __("View All","digi-theme") ?></a></div>
            <?php else: ?>
            <ul>
                <?php foreach ( $events as $post): ?>
                <li>
                    <div class="event-details">
                        <div class="month-day">
                            <?= tribe_get_start_date($event = null, $display_time = true, $date_format = 'F | j |', $timezone = null); ?>
                        </div>
                        <div class="clock">
                            <?= tribe_get_start_date($event = null, $display_time = true, $date_format = 'g:i a', $timezone = null); ?><br>
                            <?= "to " . tribe_get_end_date($event = null, $display_time = true, $date_format = 'g:i a', $timezone = null); ?>
                        </div>
                    </div>
                    <a href="<?= $post->guid ?>" title="<?= $post->post_title; ?>"><?= $post->post_title;; ?></a></a>
                </li>
                <?php endforeach; ?>
            </ul>
        <br>
        <div class="morebutton"><a href="//newsroom.aua.am/events/category/environment/" target="_blank"><?= __("View All","digi-theme") ?></a></div>
        <?php endif; ?>
        <?php restore_current_blog(); ?>
        </section>
    </div>
</div><!-- TABS -->