<div class="colored-box no-border-box">
    <div class="colored-box-title">
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
    </div>
    <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
</div><!-- Colored Box -->