<?php get_header(); ?>

<main class="projects-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1>Talks And Seminars</h1>

        <section class="talks-projects spacing">
        <div class="colored-boxes d-flex">

        <?php
        if (have_posts()) :
        while (have_posts()) : the_post(); 
          $date = get_field("date"); ?>
          
              <div class="colored-box no-border-box">
                  <div class="colored-box-title">
                      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                  </div>
                  <p>
                  <span class="talks-sems-date"><?= $date ?></span>
                  <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  </p>
              </div><!-- Colored Box -->
          <?php endwhile;
        endif; ?>

        </div><!-- Colored boxes -->
        <div class="pagination-links">
          <?php echo paginate_links( $args ); ?>
        </div>
      </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>
