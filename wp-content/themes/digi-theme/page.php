<?php get_header(); ?>

<div class="container"><?php
	if (have_posts()) :
		while (have_posts()) :
			the_post(); ?>
      <?php custom_breadcrumbs(); ?>
			<?php the_content(); ?><?php
		endwhile;
	endif; ?>
</div>

<?php get_footer(); ?>
