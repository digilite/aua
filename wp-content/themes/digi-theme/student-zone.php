<?php
/*
* Template Name: Student Zone
*/

get_header(); ?>

<main class="student-zone-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?= __("Student Zone on Enviornmental Topics","digi-theme") ?></h1>
        
        <?php if( have_rows('icon_text_box') ): ?>
          <div class="icon-text-container">
            <?php while( have_rows('icon_text_box') ): the_row(); 
                $icon_url = get_sub_field('icon');
                $title = get_sub_field('title');
                $descrition = get_sub_field('descrition');
                $links_to = get_sub_field('links_to');
                ?>
                <div class="icon-text-box">
                <a href="<?= $links_to ?>" class="icon-text-box-link"></a>
                <div class="icon-text-icon">
                  <img src="<?= $icon_url ?>" alt="<?= $title ?>">
                </div>
                <div class="icon-text-box-info">
                  <h5><?= $title ?></h5>
                  <p><?= $descrition ?></p>
                </div>
                </div><!-- icon-text-box -->
            <?php endwhile; ?>
          </div><!-- icon-text-container -->
        <?php endif; ?>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>