<?php get_header();

$tax_name = 'resources_categories';
$the_id = get_queried_object()->term_id;
$termf = get_queried_object();
$parent_id = wp_get_term_taxonomy_parent_id( $the_id, $tax_name );
$what_is = get_field( 'what_is_domlib', 'options' );
?>

<main class="collaborations-page ftm">
  <div class="container">
  <div id="plug-breadcrumbs">
      <?php if (function_exists('show_full_breadcrumb')) show_full_breadcrumb(); ?>
    </div>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php single_term_title(); ?></h1>
        <p class="domain-into-text"><?= $what_is ?></p>

        <div class="sub-subs-container">
          <?php
          $terms = get_terms([
            'taxonomy' => $tax_name,
            'hide_empty' => false,
            'parent' => $the_id,
          ]);

          foreach ($terms as $term) : ?>
              <div class="sub-subs-box">
                  <a href="<?= get_term_link($term); ?>" class="parent-sub"><?= $term->name; ?></a>
                  <?php 
                  $termss = get_terms([
                  'taxonomy' => $tax_name,
                  'hide_empty' => false,
                  'parent' => $term->term_id,
                  ]);

                  foreach ($termss as $termch) :
                  ?>
                  <a href="<?= get_term_link($term); ?>" class="sub-sub"><?= $termch->name; ?></a>
                  <?php endforeach; ?>
              </div><!-- sub-subs-box -->
          <?php endforeach; ?>
        </div><!-- sub-subs-container -->
    
      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>