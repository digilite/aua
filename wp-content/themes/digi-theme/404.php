<?php get_header(); ?>

<div class="container page-404 text-center">
	<h1>404</h1>
  <p>OOPS, something went wrong .<br>Page is not found</p>
	<a href="<?php bloginfo('url'); ?>">
		<i class="fa fa-chevron-left"></i>
		Back to Homepage
	</a>
</div>

<?php get_footer(); ?>
