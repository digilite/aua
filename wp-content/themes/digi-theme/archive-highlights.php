<?php get_header(); ?>

<main id="highlights-archive">
  <div class="container">
    <?php 
        custom_breadcrumbs();
        if (have_posts()) : 
        $current_year = date("Y");
    ?>
    <div class="cus-container">
    <h2><?= $current_year ?> HIGHLIGHTS</h2>
	<?php 
        
        while (have_posts()) : the_post();
        $high_number = get_field("number"); 
        $post_date = get_the_date( 'Y' ); ?>
    
    
    <?php
        if( $post_date != $current_year ) {
            $current_year = $post_date;
            echo "<h2>". $current_year . " HIGHLIGHTS </h2>";
        }
    ?>
        <div class="item">
            <div class="high-icon">
                <?php the_post_thumbnail(); ?>
                </div>
                <div class="high-desc">
                <span><?= $high_number ?></span>
                <p><?php the_title(); ?></p>
                <?= $post_date ?>
            </div>
        </div><!-- item -->    

    <?php endwhile; ?>
    </div>
	<?php endif; ?>

  </div><!-- Container -->
</main>

<?php get_footer(); ?>