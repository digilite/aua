<?php
/*
* Template Name: All Function Areas
*/

get_header(); ?>

<main class="function-areas-page">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <?php get_template_part("templates/hero-function-areas"); ?>
      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>
