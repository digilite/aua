<?php get_header(); ?>

<main class="projects-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php single_term_title(); echo ' ' .__("Projects","digi-theme")?></h1>

        <section class="talks-projects spacing">
          <div class="colored-boxes d-flex">
          <?php
          if (have_posts()) :
          while (have_posts()) : the_post(); 
            get_template_part( 'templates/colored-box' );
          endwhile; endif; ?>
        </div><!-- Colored boxes -->
      </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>
