<?php
/*
* Template Name: Function Areas
*/

$main_desc = get_field( "description" );
$banner_image = get_field( "banner_image" );

get_header(); ?>

<main class="function-areas-page">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <?php if($banner_image) : ?>
        <img src="<?= $banner_image ?>" class="function-area-banner" alt="Function Area Banner">
        <?php endif; ?>
        <div class="row mb-4">
          <div class="col-md-12 <?php echo (empty($banner_image)) ? 'col-lg-6' : ''; ?>">
            <h1><?php the_title(); ?></h1>
            <p><?= $main_desc ?></p>
          </div>
          <?php if( empty($banner_image) ) : ?>
          <div class="col-md-12 col-lg-6">
            <img class="main-fnc-img" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
          </div>
          <?php endif; ?>
        </div>

        <div class="accordion" id="accordionExample">
        <?php get_template_part("content"); ?>
        </div>
        
        </div>
      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>
