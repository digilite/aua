<?php
// ADD CUSTOM TAXONOMY IF NEEDED
/*
function custom_taxonomy_type() {
	$labels = [
		"name"              => _x("Event Type", "taxonomy general name"),
		"singular_name"     => _x("Event Type", "taxonomy singular name"),
		"search_items"      => __("Search Event Types"),
		"all_items"         => __("All Event Types"),
		"parent_item"       => __("Parent Event Type"),
		"parent_item_colon" => __("Parent Event Type:"),
		"edit_item"         => __("Edit Event Type"),
		"update_item"       => __("Update Event Type"),
		"add_new_item"      => __("Add New Event Type"),
		"new_item_name"     => __("New Event Type"),
		"menu_name"         => __("Event Type"),
	];

	$args = [
		"hierarchical"      => true,
		"labels"            => $labels,
		"show_ui"           => true,
		"show_admin_column" => true,
		"query_var"         => true,
		"rewrite"           => ["slug" => "event-type"],
	];

	register_taxonomy("event-type", ["conjunctures"], $args);
}
add_action("init", "custom_taxonomy_type", 0);
*/

function ess_custom_taxonomy_Item()  {
  $labels = array(
      'name'                       => 'Categories',
      'singular_name'              => 'Category',
      'menu_name'                  => 'Categories',
      'all_items'                  => 'All Categories',
      'parent_item'                => 'Parent Category',
      'parent_item_colon'          => 'Parent Category:',
      'new_item_name'              => 'New Category Name',
      'add_new_item'               => 'Add New Category',
      'edit_item'                  => 'Edit Category',
      'update_item'                => 'Update Category',
      'separate_items_with_commas' => 'Separate Category with commas',
      'search_items'               => 'Search Categories',
      'add_or_remove_items'        => 'Add or remove Categories',
      'choose_from_most_used'      => 'Choose from the most used Categories',
  );
  $args = array(
      'labels'                     => $labels,
      'hierarchical'               => true,
      'public'                     => true,
      'show_ui'                    => true,
      'show_admin_column'          => true,
      'show_in_nav_menus'          => true,
      'show_tagcloud'              => true
  );
  register_taxonomy( 'resources_categories', 'resources_cpt', $args );
  }
  add_action( 'init', 'ess_custom_taxonomy_item', 0 );


//Project Status
  function status_custom_taxonomy_Item()  {
    $labels = array(
        'name'                       => 'Projects Status',
        'singular_name'              => 'Project Status',
        'menu_name'                  => 'Projects Status',
        'all_items'                  => 'All Projects Status',
        'parent_item'                => 'Parent Projects Status',
        'parent_item_colon'          => 'Parent Projects Status:',
        'new_item_name'              => 'New Projects Status Name',
        'add_new_item'               => 'Add New Projects Status',
        'edit_item'                  => 'Edit Projects Status',
        'update_item'                => 'Update Projects Status',
        'separate_items_with_commas' => 'Separate Projects Status with commas',
        'search_items'               => 'Search Projects Status',
        'add_or_remove_items'        => 'Add or remove Projects Status',
        'choose_from_most_used'      => 'Choose from the most used Projects Status',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true
    );
    register_taxonomy( 'project_status', 'projects', $args );
    }
    add_action( 'init', 'status_custom_taxonomy_Item', 0 );

//Academics
function academics_custom_taxonomy_Item()  {
  $labels = array(
      'name'                       => 'Academics Categories',
      'singular_name'              => 'Academic Category',
      'menu_name'                  => 'Academics Categories',
      'all_items'                  => 'All Academics Categories',
      'parent_item'                => 'Parent Academics Categories',
      'parent_item_colon'          => 'Parent Academics Categories:',
      'new_item_name'              => 'New Academics Categories Name',
      'add_new_item'               => 'Add New Academics Categories',
      'edit_item'                  => 'Edit Academics Categories',
      'update_item'                => 'Update Academics Categories',
      'separate_items_with_commas' => 'Separate Academics Categories with commas',
      'search_items'               => 'Search Academics Categories',
      'add_or_remove_items'        => 'Add or remove Academics Categories',
      'choose_from_most_used'      => 'Choose from the most used Academics Categories',
  );
  $args = array(
      'labels'                     => $labels,
      'hierarchical'               => true,
      'public'                     => true,
      'show_ui'                    => true,
      'show_admin_column'          => true,
      'show_in_nav_menus'          => true,
      'show_tagcloud'              => true,
      'rewrite' => array( 'slug' => 'academic')
  );
  register_taxonomy( 'academics_cats', 'academics_cpt', $args );
  }
  add_action( 'init', 'academics_custom_taxonomy_Item', 0 );

