<?php
// PROJECTS CUSTOM POST TYPE
function custom_post_type_projects() {
	$labels = [
		"name"               => _x("Projects", "post type general name"),
		"singular_name"      => _x("Project", "post type singular name"),
		"add_new"            => _x("Add New", "Project"),
		"add_new_item"       => __("Add New Project"),
		"edit_item"          => __("Edit Project"),
		"new_item"           => __("New Project"),
		"all_items"          => __("All Projects"),
		"view_item"          => __("View Project"),
		"search_items"       => __("Search Projects"),
		"not_found"          => __("No Projects found"),
		"not_found_in_trash" => __("No Projects found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Projects"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Projects specific data",
		"public"        => true,
		"menu_position" => 5,
		"taxonomies"	=> array('post_tag'),
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
		"menu_icon" 	=> "dashicons-admin-site-alt3",
	];
	register_post_type("projects", $args);
}
add_action("init", "custom_post_type_projects");


// TALKS AND SEMINARS CUSTOM POST TYPE
function custom_post_type_talk_seminars() {
	$labels = [
		"name"               => _x("Talks And Seminars", "post type general name"),
		"singular_name"      => _x("Talks And Seminars", "post type singular name"),
		"add_new"            => _x("Add New", "Talks And Seminars"),
		"add_new_item"       => __("Add New Talks And Seminars"),
		"edit_item"          => __("Edit Talks And Seminars"),
		"new_item"           => __("New Talks And Seminars"),
		"all_items"          => __("All Talks And Seminars"),
		"view_item"          => __("View Talks And Seminars"),
		"search_items"       => __("Search Talks And Seminars"),
		"not_found"          => __("No Talks And Seminars found"),
		"not_found_in_trash" => __("No Talks And Seminars found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Talks And Seminars"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Talks And Seminars specific data",
		"public"        => true,
		"menu_position" => 5,
		"taxonomies" 	=> array('post_tag'),
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
		"menu_icon" 	=> "dashicons-welcome-learn-more",
	];
	register_post_type("talk_seminars", $args);
}
add_action("init", "custom_post_type_talk_seminars");


// RESOURCES CUSTOM POST TYPE
function custom_post_type_resources_cpt() {
	$labels = [
		"name"               => _x("Resources", "post type general name"),
		"singular_name"      => _x("Resource", "post type singular name"),
		"add_new"            => _x("Add New", "Resource"),
		"add_new_item"       => __("Add New Resource"),
		"edit_item"          => __("Edit Resource"),
		"new_item"           => __("New Resource"),
		"all_items"          => __("All Resources"),
		"view_item"          => __("View Resource"),
		"search_items"       => __("Search Resources"),
		"not_found"          => __("No Resources found"),
		"not_found_in_trash" => __("No Resources found in the Trash"),
		"parent_item_colon"  => "",
		"menu_name"          => "Resources"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Resources specific data",
		"public"        => true,
		"menu_position" => 5,
		"taxonomies" 	=> array('post_tag'),
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
		"rewrite" => array( "slug" => "resources" ),
	];
	register_post_type("resources_cpt", $args);
}
add_action("init", "custom_post_type_resources_cpt");

//ACADEMICS CUSTOM POST TYPE
function custom_post_type_academics_cpt() {
	$labels = [
		"name"               => _x("Academics", "post type general name"),
		"singular_name"      => _x("Academic", "post type singular name"),
		"add_new"            => _x("Add New", "Academics"),
		"add_new_item"       => __("Add New Academics"),
		"edit_item"          => __("Edit Academics"),
		"new_item"           => __("New Academics"),
		"all_items"          => __("All Academics"),
		"view_item"          => __("View Academics"),
		"search_items"       => __("Search Academics"),
		"not_found"          => __("No Academics found"),
		"not_found_in_trash" => __("No Academics found in the Trash"),
		"parent_item_colon"  => "",
		"menu_name"          => "Academics"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Academics specific data",
		"public"        => true,
		"menu_position" => 5,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
		"rewrite" => array( "slug" => "academics"),
	];
	register_post_type("academics_cpt", $args);
}
add_action("init", "custom_post_type_academics_cpt");


// HIGHLIGHTS CUSTOM POST TYPE
function custom_post_type_highlights() {
	$labels = [
		"name"               => _x("Highlights", "post type general name"),
		"singular_name"      => _x("Highlight", "post type singular name"),
		"add_new"            => _x("Add New", "Highlight"),
		"add_new_item"       => __("Add New Highlight"),
		"edit_item"          => __("Edit Highlight"),
		"new_item"           => __("New Highlight"),
		"all_items"          => __("All Highlights"),
		"view_item"          => __("View Highlight"),
		"search_items"       => __("Search Highlights"),
		"not_found"          => __("No Highlights found"),
		"not_found_in_trash" => __("No Highlights found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Highlights"
	];

	$args = [
		"labels"        => $labels,
		"description"   => "Holds our Highlight specific data",
		"public"        => true,
		"menu_position" => 5,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
		"menu_icon" 	=> "dashicons-clipboard",
	];
	register_post_type("highlights", $args);
}
add_action("init", "custom_post_type_highlights");

?>