<?php
/*
* Template Name: Talks And Seminars
*/

get_header();?>

<main class="talk-seminars-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1>Talks and Seminars</h1>
        <section class="talks-projects">
        <div class="colored-boxes d-flex">
          <div class="colored-box">
            <div class="colored-box-title">
                <h3>Initiatives Seminars</h3>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
                <h3>Initiatives Seminars</h3>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
                <h3>Initiatives Seminars</h3>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
                <h3>Initiatives Seminars</h3>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
                <h3>Initiatives Seminars</h3>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
                <h3>Initiatives Seminars</h3>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
                <h3>Initiatives Seminars</h3>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
                <h3>Initiatives Seminars</h3>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

        </div><!-- Colored boxes -->

      </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>






<?php get_footer(); ?>
