<?php
/*
* Template Name: Highlights
*/

get_header();?>

<main class="highlights-page">
  <div class="container">
  <?php custom_breadcrumbs(); ?>
  <section class="highlight-section">
    <h2>2020/2021 HIGHLIGHTS</h2>
    <div class="single-section">
    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->
    </div>
  </section>

  <section class="highlight-section">
    <h2>2019/2020 HIGHLIGHTS</h2>
    <div class="single-section">
    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->

    <div class="item">
      <div class="high-icon">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vector.png" alt="Highlight Icon">
      </div>
      <div class="high-desc">
        <span>17</span>
        <p>graduate and undergraduate courses taught at AUA with 601 students enrolled</p>
      </div>
    </div><!-- item -->
    </div>
  </section>
  </div><!-- Container -->
</main>






<?php get_footer(); ?>
