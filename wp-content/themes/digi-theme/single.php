<?php get_header();

$terms = get_field('tag');
$projects_title = __("Projects","digi-theme");
$talk_sems_title = __("Talks And Seminars","digi-theme");
$res = __("Resources","digi-theme");
?>

<main class="projects-page ftm">
  <div class="container">
    <div id="plug-breadcrumbs">
      <?php if (function_exists('show_full_breadcrumb')) show_full_breadcrumb(); ?>
    </div>
    <div class="row">
      <div class="col-md-3">
        <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php the_title(); ?></h1>
        <section class="spacing">

        <?php
        if (have_posts()) :
	      while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-md-7">
                    <?php
                    the_content();
                    if( has_term('collaborations', 'category') ) : 

                      $postst = get_posts(array(
                        'post_type' => 'talk_seminars',
                        'numberposts' => -1,
                        'tax_query' => array(
                          array(
                            'taxonomy' => 'post_tag',
                            'field' => 'term_id', 
                            'terms' => $terms,
                          )
                        )
                      )); 
                      if( $postst ) : ?>
                      <h3><?= $talk_sems_title ?></h3>
                      <ul class="collab-post-lists">
                      <?php foreach ( $postst as $post ) : ?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                      <?php endforeach; 
                      wp_reset_postdata();?>
                      </ul>
                      <?php endif;
                    
                      $postsp = get_posts(array(
                        'post_type' => 'projects',
                        'numberposts' => -1,
                        'tax_query' => array(
                          array(
                            'taxonomy' => 'post_tag',
                            'field' => 'term_id', 
                            'terms' => $terms,
                          )
                        )
                      )); 
                      if( $postsp ) : ?>
                      <h3><?= $projects_title ?></h3>
                      <ul class="collab-post-lists">
                      <?php foreach ( $postsp as $post ) : ?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                      <?php endforeach; 
                      wp_reset_postdata();?>
                      </ul>
                      <?php endif;

                    endif; ?>
                    
                </div>
                <div class="col-md-5 talk-single-img">
                    <?php if( has_post_thumbnail() ) { 
                        the_post_thumbnail();
                     } ?>
                </div>
            </div><!-- row -->
        <?php endwhile; endif; ?>
      </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>

<?php get_footer(); ?>
