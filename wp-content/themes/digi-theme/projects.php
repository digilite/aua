<?php
/*
* Template Name: Projects
*/

get_header();?>

<main class="projects-page ftm">
  <div class="container">
    <?php custom_breadcrumbs(); ?>
    <div class="row">
      <div class="col-md-3">
      <?php get_sidebar();?>
      </div>
      <div class="col-md-9">
        <h1><?php the_title(); ?></h1>
        <div class="projects-page-buttons">
          <a href="#" class="button white-button">
            <img src="<?php echo get_template_directory_uri(); ?>/img/rotate-arrow.png" alt="">
            <span>Ongoing<br>Projects</span>
          </a>
          <a href="#" class="button white-button">
            <img src="<?php echo get_template_directory_uri(); ?>/img/vector123.png" alt="">
            <span>Completed<br>projects</span>
          </a>
        </div>
        <section class="talks-projects">
        <div class="colored-boxes d-flex">
          <div class="colored-box">
            <div class="colored-box-title">
              <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/POntos.png" alt="Project image"></a>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
              <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/POntos.png" alt="Project image"></a>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
              <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/POntos.png" alt="Project image"></a>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/POntos.png" alt="Project image"></a>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
              <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/POntos.png" alt="Project image"></a>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
              <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/POntos.png" alt="Project image"></a>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
              <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/POntos.png" alt="Project image"></a>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

          <div class="colored-box">
            <div class="colored-box-title">
              <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/POntos.png" alt="Project image"></a>
            </div>
            <p>Managing
            Biodiversity
            in Armenia</p>
          </div><!-- Colored Box -->

        </div><!-- Colored boxes -->

      </section>

      </div><!-- col-md-9 -->
    </div><!-- Row -->

  </div><!-- Container -->
</main>



<?php get_footer(); ?>
